import React from "react"
import { Navbar, Nav } from "react-bootstrap"
import SocialMedia from "../SocialMedia"
import "./Menu.scss"

export default function Menu() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="menu" variant="dark" sticky="top">
      <Navbar.Brand href="#home">Filmyk</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="#features">Home</Nav.Link>
          <Nav.Link href="#pricing">Programación</Nav.Link>
          <Nav.Link href="#pricing">Fotografía</Nav.Link>
          <Nav.Link href="#pricing">Audio</Nav.Link>
          <Nav.Link href="#pricing">Vídeo</Nav.Link>
        </Nav>
        <Nav>
          <SocialMedia />
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

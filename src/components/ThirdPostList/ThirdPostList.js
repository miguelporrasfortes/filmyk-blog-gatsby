import React from "react"
import { Icon } from "semantic-ui-react"
import { Jumbotron, Container, Card, Row, Col } from "react-bootstrap"
import { Link } from "gatsby"
import moment from "moment"
import "moment/locale/es"
import "./ThirdPostList.scss"

export default function ThirdPostList(props) {
  const { posts } = props
  return (
    <Jumbotron className="light">
      <Container>
        <Row className="third-post-list">
          <Col xs={6} md={4}>
            <Card key={posts[5].id}>
              <Link to={`/${posts[5].url}`}>
                <Card.Img src="https://picsum.photos/208/109?random=5" />
                <Card.Body>
                  <Card.Title>{posts[5].title}</Card.Title>
                </Card.Body>
                <Card.Footer>
                  <small className="text-muted">
                    <Icon name="calendar alternate outline" />
                    {moment(posts[5].createdAt).format("LL")}
                  </small>
                </Card.Footer>
              </Link>
            </Card>
          </Col>
          <Col xs={6} md={4}>
            <Card key={posts[6].id}>
              <Link to={`/${posts[6].url}`}>
                <Card.Img src="https://picsum.photos/208/109?random=6" />
                <Card.Body>
                  <Card.Title>{posts[6].title}</Card.Title>
                </Card.Body>
                <Card.Footer>
                  <small className="text-muted">
                    <Icon name="calendar alternate outline" />
                    {moment(posts[6].createdAt).format("LL")}
                  </small>
                </Card.Footer>
              </Link>
            </Card>
          </Col>
          <Col xs={6} md={4}>
            <Card key="widget">
              <Card.Body>
                <Card.Title>Aquí irá un Widget</Card.Title>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  )
}

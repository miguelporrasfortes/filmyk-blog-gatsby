import React from "react"
import { Icon } from "semantic-ui-react"
import { Jumbotron, Container, Row, Col, Image } from "react-bootstrap"
import { Link } from "gatsby"
import moment from "moment"
import { map } from "lodash"
import "moment/locale/es"
import "./LastPost.scss"

export default function LastPost(props) {
  const { posts } = props
  return (
    <Jumbotron className="light">
      <Container>
        {map(posts, post => (
          <Row className="first-post-list">
            <Col xs={6} md={6}>
              <Link to={`/${post.url}`}>
                <Image src="https://picsum.photos/450/237?random=1" fluid />
              </Link>
            </Col>
            <Col xs={6} md={6}>
              <Link to={`/${post.url}`}>
                <h1>{post.title}</h1>
              </Link>
              <small className="text-muted">
                <Icon name="calendar alternate outline" />
                {moment(post.createdAt).format("LL")}
              </small>
            </Col>
          </Row>
        ))}
      </Container>
    </Jumbotron>
  )
}

import React from "react"
import { Jumbotron, Image, Row, Col } from "react-bootstrap"
import mockup from "../../images/filmyk-mockup-jumbotron.png"
import "./Cover.scss"

export default function Cover() {
  return (
    <Jumbotron fluid className="light">
      <Row className="cover">
        <Col>
          <h1>
            Bienvenido a <span id="title">Filmyk</span>
          </h1>
          <p id="text">Un lugar donde compartir conocimiento</p>
        </Col>
        <Col>
          <Image src={mockup} fluid />
        </Col>
      </Row>
    </Jumbotron>
  )
}

import React from "react"
import { Container, Row, Col } from "react-bootstrap"
import { Link } from "gatsby"
import SocialMedia from "../SocialMedia"
import "./PageFooter.scss"

export default function PageFooter() {
  return (
    <Container className="dark">
      <Row className="page-footer">
        <Col xs={12} md={4}>
          <h3>ABOUT FILMYK</h3>
          <p>
            Start writing, no matter what. The water does not flow until the
            faucet is turned on.
          </p>
        </Col>
        <Col xs={12} md={4}>
          <h3>QUICK LINK</h3>
          <ul className="linkList">
            <li>
              <Link>About me</Link>
            </li>
            <li>
              <Link>Help and Support</Link>
            </li>
            <li>
              <Link>Licensing Policy</Link>
            </li>
            <li>
              <Link>Refund Policy</Link>
            </li>
            <li>
              <Link>Find me</Link>
            </li>
            <li>
              <Link>Contact</Link>
            </li>
          </ul>
        </Col>
        <Col xs={12} md={4}>
          <h3>REDES SOCIALES</h3>
          <SocialMedia />
        </Col>
      </Row>
    </Container>
  )
}

import React from "react"
import { Icon } from "semantic-ui-react"
import { Container, Card, Row, Col } from "react-bootstrap"
import { Link } from "gatsby"
import moment from "moment"
import "moment/locale/es"
import "./FirstPostList.scss"

export default function FirsPostList(props) {
  const { posts } = props

  return (
    <Container className="dark">
      <Row className="first-post-list">
        <Col xs={6} md={8}>
          <Card key={posts[0].id}>
            <Link to={`/${posts[0].url}`}>
              <Card.Img src="https://picsum.photos/450/237?random=1" />
              <Card.ImgOverlay>
                <Card.Title>{posts[0].title}</Card.Title>
              </Card.ImgOverlay>
            </Link>
          </Card>
        </Col>
        <Col xs={6} md={4}>
          <Card key={posts[1].id}>
            <Link to={`/${posts[1].url}`}>
              <Card.Img src="https://picsum.photos/208/109?random=1" />
              <Card.Body>
                <Card.Title>{posts[1].title}</Card.Title>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  <Icon name="calendar alternate outline" />
                  {moment(posts[1].createdAt).format("LL")}
                </small>
              </Card.Footer>
            </Link>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

import React from "react"
import { Icon } from "semantic-ui-react"
import { Jumbotron, Container, Card, Row, Col } from "react-bootstrap"
import { Link } from "gatsby"
import moment from "moment"
import "moment/locale/es"
import "./ForthPostList.scss"

export default function ForthPostList(props) {
  const { posts } = props
  return (
    <Jumbotron className="light">
      <Container>
        <Row className="forth-post-list">
          <Col xs={6} md={4}>
            <Card key={posts[7].id}>
              <Link to={`/${posts[7].url}`}>
                <Card.Img src="https://picsum.photos/208/109?random=7" />
                <Card.Body>
                  <Card.Title>{posts[7].title}</Card.Title>
                </Card.Body>
                <Card.Footer>
                  <small className="text-muted">
                    <Icon name="calendar alternate outline" />
                    {moment(posts[7].createdAt).format("LL")}
                  </small>
                </Card.Footer>
              </Link>
            </Card>
          </Col>
          <Col xs={6} md={4}>
            <Card key={posts[8].id}>
              <Link to={`/${posts[8].url}`}>
                <Card.Img src="https://picsum.photos/208/109?random=8" />
                <Card.Body>
                  <Card.Title>{posts[8].title}</Card.Title>
                </Card.Body>
                <Card.Footer>
                  <small className="text-muted">
                    <Icon name="calendar alternate outline" />
                    {moment(posts[8].createdAt).format("LL")}
                  </small>
                </Card.Footer>
              </Link>
            </Card>
          </Col>
          <Col xs={6} md={4}>
            <Card key="widget">
              <Card.Title>La lista de redes sociales</Card.Title>
            </Card>
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  )
}

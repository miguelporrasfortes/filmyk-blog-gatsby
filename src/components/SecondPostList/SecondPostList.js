import React from "react"
import { Icon } from "semantic-ui-react"
import { Container, Card, Row, Col } from "react-bootstrap"
import { Link } from "gatsby"
import moment from "moment"
import "moment/locale/es"
import "./SecondPostList.scss"

export default function SecondPostList(props) {
  const { posts } = props
  return (
    <Container className="dark">
      <Row className="second-post-list">
        <Col xs={6} md={4}>
          <Card key={posts[2].id}>
            <Link to={`/${posts[2].url}`}>
              <Card.Img src="https://picsum.photos/208/109?random=2" />
              <Card.Body>
                <Card.Title>{posts[2].title}</Card.Title>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  <Icon name="calendar alternate outline" />
                  {moment(posts[2].createdAt).format("LL")}
                </small>
              </Card.Footer>
            </Link>
          </Card>
        </Col>
        <Col xs={6} md={4}>
          <Card key={posts[3].id}>
            <Link to={`/${posts[3].url}`}>
              <Card.Img src="https://picsum.photos/208/109?random=3" />
              <Card.Body>
                <Card.Title>{posts[3].title}</Card.Title>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  <Icon name="calendar alternate outline" />
                  {moment(posts[3].createdAt).format("LL")}
                </small>
              </Card.Footer>
            </Link>
          </Card>
        </Col>
        <Col xs={6} md={4}>
          <Card key={posts[4].id}>
            <Link to={`/${posts[4].url}`}>
              <Card.Img src="https://picsum.photos/208/109?random=4" />
              <Card.Body>
                <Card.Title>{posts[4].title}</Card.Title>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  <Icon name="calendar alternate outline" />
                  {moment(posts[4].createdAt).format("LL")}
                </small>
              </Card.Footer>
            </Link>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

import React from "react"
import BlogLayout from "../../layouts/BlogLayout"
import TransformOembedToIframe from "../../utils/TransformOembedToIframe"
import { Container } from "react-bootstrap"
import "./post.scss"

export default function Post(props) {
  const {
    pageContext: { data: post },
  } = props

  return (
    <BlogLayout className="post">
      <Container className="dark">
        <h1>{post.title}</h1>
        <div className="markdown-body">
          <div
            dangerouslySetInnerHTML={{
              __html: TransformOembedToIframe(post.content),
            }}
          />
        </div>
      </Container>
    </BlogLayout>
  )
}

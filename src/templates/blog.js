import React from "react"
import Menu from "../components/Menu"
import Cover from "../components/Cover"
import { graphql } from "gatsby"
import BlogLayout from "../layouts/BlogLayout"
import FirstPostList from "../components/FirstPostList"
import loadable from "@loadable/component"

export default function Blog(props) {
  const { data } = props
  const posts = data.allStrapiPost.nodes
  const SecondPostList = loadable(() => import("../components/SecondPostList"))
  const ThirdPostList = loadable(() => import("../components/ThirdPostList"))
  const ForthPostList = loadable(() => import("../components/ForthPostList"))
  const LastPost = loadable(() => import("../components/LastPost"))
  const PageFooter = loadable(() => import("../components/PageFooter"))

  return (
    <BlogLayout>
      <Menu />
      <Cover />
      <FirstPostList posts={posts} />
      <SecondPostList posts={posts} />
      <ThirdPostList posts={posts} />
      <ForthPostList posts={posts} />
      <LastPost posts={posts} />
      <PageFooter />
    </BlogLayout>
  )
}

export const query = graphql`
  query($skip: Int!, $limit: Int!) {
    allStrapiPost(
      skip: $skip
      limit: $limit
      sort: { fields: createdAt, order: DESC }
    ) {
      nodes {
        id
        title
        url
        content
        createdAt
        miniature {
          url
        }
      }
    }
  }
`

import React from "react"
//import { Container } from "react-bootstrap"
import "./BlogLayout.scss"

export default function BlogLayout(props) {
  const { children } = props
  return <div className="blog-layout">{children}</div>
}
